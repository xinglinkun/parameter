^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Changelog for package rclc_parameter
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

2.0.4 (2021-08-19)
------------------
* Added Quality Declaration Statement

2.0.3 (2021-07-26)
------------------
* Added test dependencies for rclc_parameter

2.0.2 (2021-07-17)
------------------
* Removed shared from rclc_parameter
* Ensure clean message when set_parameter
* Added QoS entity creation API
* Updated CMakeLists.txt
* Major vesion bump was neccessary because API change in rcl_lifecycle package

0.1.0 (2021-05-27)
------------------
* Initial release