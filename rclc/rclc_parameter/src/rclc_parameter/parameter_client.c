#pragma once
#include "rclc_parameter/parameter_client.h"
#define M_GET_PARAMETERS "/get_parameters"
#define M_GET_PARAMETERS_TYPES "/get_parameter_types"
#define M_SET_PARAMETERS "/set_parameters"
#define M_SET_PARAMETERS_ATOMICALLY  "/set_parameters_atomically"
#define M_DESCRIBE_PARAMETERS "/describe_parameters"
#define M_LIST_PARAMETERS "/list_parameters"
#define MAX_LENTH 100
#define RCLC_SHOW_DEBUG 0

bool IS_CALL_BACK;
bool IS_GET;
bool IS_SET;
size_t dynamic_Size;
size_t FAIL_COUNT;
uint8_t type_Id[MAX_LENTH];
Basic_Data_Set_ get_Data;
Parameter_Set_ set_Data;
Parameter_Set_* dynamic_Data;
int get_Type[MAX_LENTH];

bool parameter_init = false;
pthread_t parameter_Change_Thread;
rcl_node_t parameter_Change_Node;
rcl_subscription_t parameter_Change_Subscription;
rclc_executor_t parameter_Change_Executor;

//qos profile
const rmw_qos_profile_t rcl_msg_qos_profile_status_default =
{
  RMW_QOS_POLICY_HISTORY_KEEP_LAST,
  1000,
  RMW_QOS_POLICY_RELIABILITY_RELIABLE,
  RMW_QOS_POLICY_DURABILITY_VOLATILE,
  RMW_QOS_DEADLINE_DEFAULT,
  RMW_QOS_LIFESPAN_DEFAULT,
  RMW_QOS_POLICY_LIVELINESS_SYSTEM_DEFAULT,
  RMW_QOS_LIVELINESS_LEASE_DURATION_DEFAULT,
  false
};

//parameter type
typedef enum Parameter_Type
{
    Parameter_BOOL = 1,
    Parameter_INT,
    Parameter_DOUBLE,
    Parameter_STRING,
    Parameter_Array_BYTE,
    Parameter_Array_BOOL,
    Parameter_Array_INT,
    Parameter_Array_DOUBLE,
    Parameter_Array_STRING
}Parameter_Type;

//FAIL_COUNT
void fail_count_judge()
{
    if (FAIL_COUNT == 5) {
        printf("request is error, please open the service!\n");
    }
}

//create service name
char* create_service_name(const char *remote_nodename, const char *servicename)
{
    static char get_service_name[RCLC_PARAMETER_MAX_STRING_LENGTH];
    memset(get_service_name, 0, RCLC_PARAMETER_MAX_STRING_LENGTH);
    memcpy(get_service_name, remote_nodename, strlen(remote_nodename) + 1);
    memcpy((get_service_name + strlen(remote_nodename)), servicename, strlen(servicename) + 1);
    return get_service_name;
}

//enum type support
typedef enum Type_Support{
    Get_Parameters_ = 0,
    Get_Parameter_Types_,
    Set_Parameters_,
    Set_Parameters_Atomically_,
    Describe_Parameters_,
    List_Parameters_
} Type_Support;

//get service_type_support
const rosidl_service_type_support_t * rclc_param_type_support(Type_Support support_)
{
    switch (support_)
    {
    case Get_Parameters_:
        return ROSIDL_GET_SRV_TYPE_SUPPORT(rcl_interfaces, srv, GetParameters);
    case Get_Parameter_Types_:
        return ROSIDL_GET_SRV_TYPE_SUPPORT(rcl_interfaces, srv, GetParameterTypes);
    case Set_Parameters_:
        return ROSIDL_GET_SRV_TYPE_SUPPORT(rcl_interfaces, srv, SetParameters);
    case Set_Parameters_Atomically_:
        return ROSIDL_GET_SRV_TYPE_SUPPORT(rcl_interfaces, srv, SetParametersAtomically);
    case Describe_Parameters_:
        return ROSIDL_GET_SRV_TYPE_SUPPORT(rcl_interfaces, srv, DescribeParameters);
    case List_Parameters_:
        return ROSIDL_GET_SRV_TYPE_SUPPORT(rcl_interfaces, srv, ListParameters);
    default:
        break;
    }
    return NULL;
}

//send ret judge
bool rclc_send_judge(rcl_ret_t rc, char* service)
{
    if (rc == RCL_RET_INVALID_ARGUMENT) {
#if RCLC_SHOW_DEBUG
        printf("%s: Arguments are invalid\n", service);
#endif
    } else if (rc == RCL_RET_ERROR) {
#if RCLC_SHOW_DEBUG
        printf("%s: An unspecified error occurs\n", service);
#endif
    } else if (rc == RCL_RET_CLIENT_INVALID) {
#if RCLC_SHOW_DEBUG
        printf("%s: RCL_RET_CLIENT_INVALID\n", service);
#endif
    } else {
#if RCLC_SHOW_DEBUG
        printf("%s: Request Successfully\n", service);
#endif
        return true;
    }
    return false;
}

//clean up
void rclc_clean(
    rcl_node_t* node,
    rcl_client_t* client
)
{
    rcl_client_fini(client, node);
    rcl_node_fini(node);
}

//check client valid
void rclc_check_client_or_node(rcl_client_t* client, rcl_node_t* node)
{
    bool ret = false;
    if (client != NULL)
        ret = rcl_client_is_valid(client);
    if (node != NULL) 
        ret = rcl_node_is_valid(node);
    if (!ret) 
        printf("[parameter_client::rclc_check_client_or_node] node or client is not valid\n");
}

//get_parameter callback
void get_parameter_callback(const void * msg)
{
    rcl_interfaces__srv__GetParameters_Response* res = (rcl_interfaces__srv__GetParameters_Response*)msg;
    size_t size = res->values.size;
    for(size_t i = 0; i < size; i++) {
        if (i < 100)
            type_Id[i] = res->values.data[i].type;
    }
    IS_CALL_BACK = true;
    IS_GET = true;
}

//get_parameters callback
void get_parameters_callback(const void * msg)
{
    rcl_interfaces__srv__GetParameters_Response* res = (rcl_interfaces__srv__GetParameters_Response*)msg;
    size_t size = res->values.size;
#if RCLC_SHOW_DEBUG
    printf("[get_parameters_callback] size: %ld\n",res->values.size);
#endif

    for(size_t i = 0; i < size; i++) {

#if RCLC_SHOW_DEBUG
        printf("{ \n");
        printf("bool_value: %d\n", res->values.data[i].bool_value);
        printf("bool_array_value: [ ");
        for (size_t j = 0; j < res->values.data[i].bool_array_value.size; j++)
            printf("%d ",res->values.data[i].bool_array_value.data[j]);
        printf(" ]\n");
        printf("double_value: %f\n", res->values.data[i].double_value);

        printf("byte_array_value: [ ");
        for (size_t j = 0; j < res->values.data[i].byte_array_value.size; j++)
            printf("%d ",res->values.data[i].byte_array_value.data[j]);
        printf(" ]\n");
        printf("integer_value: %ld\n", res->values.data[i].integer_value);

        printf("double_array_value: [ ");
        for (size_t j = 0; j < res->values.data[i].double_array_value.size; j++)
            printf("%f ",res->values.data[i].double_array_value.data[j]);
        printf(" ]\n");

        printf("string_value: %s\n", res->values.data[i].string_value.data);

        printf("integer_array_value: [ ");
        for (size_t j = 0; j < res->values.data[i].integer_array_value.size; j++)
            printf("%ld ",res->values.data[i].integer_array_value.data[j]);
        printf(" ]\n");
        printf("type: %u\n", res->values.data[i].type);

        printf("string_array_value: [ ");
        for (size_t j = 0; j < res->values.data[i].string_array_value.size; j++)
            printf("%s ",res->values.data[i].string_array_value.data[j].data);
        printf(" ]\n");
        printf("} \n");
#endif
        if (i < 100)
            type_Id[i] = res->values.data[i].type;
        IS_GET = true;
        switch (type_Id[i])
        {
        case Parameter_BOOL:
            get_Data.bool_value = res->values.data[i].bool_value;
            break;
        case Parameter_INT:
            get_Data.interger_value = res->values.data[i].integer_value;
            break;
        case Parameter_DOUBLE:
            get_Data.double_value = res->values.data[i].double_value;
            printf("double: %f\n", get_Data.double_value);
            break;
        case Parameter_STRING:
            rosidl_runtime_c__String__assign(&get_Data.string_value, res->values.data[i].string_value.data);
            break;
        default:
            IS_GET = false;
            break;
        }
    }
    IS_CALL_BACK = true;
}

//get_parameters_types callback
void get_parameters_types_callback(const void* msg)
{
    rcl_interfaces__srv__GetParameterTypes_Response* res = (rcl_interfaces__srv__GetParameterTypes_Response*)msg;
    size_t size = res->types.size;
#if RCLC_SHOW_DEBUG
    printf("[get_parameters_types_callback] size: %ld\n", size);
#endif
    for (size_t i = 0; i < size; i++) {
#if RCLC_SHOW_DEBUG
        printf("type_id: %d\n",res->types.data[i]);
#endif
        get_Type[i] = res->types.data[i];
    }
    IS_CALL_BACK = true;
    IS_GET = true;
}

//list_patameters callback
void list_patameters_callback(const void* msg)
{
    rcl_interfaces__srv__ListParameters_Response* res = (rcl_interfaces__srv__ListParameters_Response*)msg;
    size_t size = res->result.names.size;
    printf("[list_patameters_callback] size: %ld\n", size);
    printf("{ \n");
    for (size_t i = 0; i < size; i++) {
        printf("names: %s\n",res->result.names.data[i].data);
    }
    printf("} \n");
    IS_CALL_BACK = true;
}

//describe_parameters callback
void describe_parameters_callback(const void* msg)
{
    rcl_interfaces__srv__DescribeParameters_Response* res = (rcl_interfaces__srv__DescribeParameters_Response*)msg;
    size_t size = res->descriptors.size;
    printf("[describe_parameters_callback] size: %ld\n", size);
    for(size_t i = 0; i < size; i++) {
        printf("{ \n");
        printf("name: %s\n", res->descriptors.data[i].name.data);
        printf("read_only: %d\n", res->descriptors.data[i].read_only);
        printf("type: %u\n", res->descriptors.data[i].type);
        printf("} \n");
    }
    IS_CALL_BACK = true;
}

//set_parameters_callback
void set_parameters_callback(const void* msg)
{
    IS_SET = false;
    rcl_interfaces__srv__SetParameters_Response* res = (rcl_interfaces__srv__SetParameters_Response*)msg;
    size_t size = res->results.size;
    bool ret = false;
    for(size_t i = 0; i < size; i++) {
        IS_SET = true;
        ret = res->results.data[i].successful;
    }
#if RCLC_SHOW_DEBUG
    printf("[set_parameters_callback]: success? %d\n", ret);
#endif
    IS_CALL_BACK = true;
}

//set_parameters_atomically_callback
void set_parameters_atomically_callback(const void* msg)
{
    rcl_interfaces__srv__SetParametersAtomically_Response* res = (rcl_interfaces__srv__SetParametersAtomically_Response*)msg;
    size_t size = res->result.reason.size;
    bool ret = res->result.successful;
#if RCLC_SHOW_DEBUG
    printf("[set_parameters_atomically_callback] \n");
    printf("reason: [");
    for(size_t i = 0; i < size; i++) {
        printf("%s, ", res->result.reason.data);
    }
    printf("] \n");
    printf("successful ? %d\n", ret);
#endif
    IS_CALL_BACK = true;
    IS_SET = true;
}

//init client and node
void rclc_param_init_client_node(
    rcl_node_t* node,
    rcl_client_t* client
)
{
    //init node and client
    *node = rcl_get_zero_initialized_node();
    *client = rcl_get_zero_initialized_client();
}

//take response message
void rclc_response_message_take(rcl_client_t* client, void* ros_response, void(*callback)(const void*), int seq)
{
    rmw_request_id_t id_t;
    rcl_ret_t rc;
    id_t.sequence_number = seq;
    rc = rcl_take_response(client, &id_t, ros_response);
    if (rc == RCL_RET_OK)
        callback(ros_response);
}

//get parameter
void rclc_param_get_parameter(
    rcl_node_t* node,
    rcl_client_t* client,
    char* node_name,
    char* remote_name,
    char** msg_request,
    size_t size,
    char* node_name_space
)
{
    rcl_ret_t rc;
    //service type
    rcl_allocator_t allocator = rcl_get_default_allocator();
    rclc_support_t support;

    // create init_options
    rclc_support_init(&support, 0, NULL, &allocator);

    // create node
    rclc_node_init_default(node, node_name, node_name_space, &support);

    // create client
    char* service_name = create_service_name(remote_name, M_GET_PARAMETERS);
    rclc_client_init_default(
      client, node,
      rclc_param_type_support(Get_Parameters_), service_name);

    //create message
    rcl_interfaces__srv__GetParameters_Request__Sequence sequence;
    rcl_interfaces__srv__GetParameters_Response__Sequence resp;
    rcl_interfaces__srv__GetParameters_Response__Sequence__init(&resp, 1);
    rcl_interfaces__srv__GetParameters_Request__Sequence__init(&sequence, 1);
    rosidl_runtime_c__String__Sequence__init(&sequence.data->names, size);
    rcl_interfaces__msg__ParameterValue__Sequence__init(&resp.data->values, size);

    for (size_t i=0; i < size; i++) {
        if (rosidl_runtime_c__String__init(&sequence.data->names.data[i]))
            rosidl_runtime_c__String__assign(&sequence.data->names.data[i], msg_request[i]);
    }

    int64_t seq = 0;
    rclc_sleep_ms(1000);   // Sleep a while to ensure DDS matching before sending request

    //request count
    IS_CALL_BACK = false;
    FAIL_COUNT = 0;

    while (!IS_CALL_BACK && FAIL_COUNT < 5) {
        rc = rcl_send_request(client, sequence.data, &seq);
        rclc_send_judge(rc, service_name);
        rclc_sleep_ms(500);
        if (seq != 0 && rc == RCL_RET_OK) {
            rclc_response_message_take(client, resp.data, get_parameter_callback, seq);
        }
        FAIL_COUNT++;
    }

    //fail count
    fail_count_judge();
    
    for (size_t i=0; i < size; i++)
        rosidl_runtime_c__String__fini(&sequence.data->names.data[i]);

    rosidl_runtime_c__String__Sequence__fini(&sequence.data->names);
    rcl_interfaces__srv__GetParameters_Response__Sequence__fini(&resp);
    rcl_interfaces__msg__ParameterValue__Sequence__fini(&resp.data->values);
    rcl_interfaces__srv__GetParameters_Request__Sequence__fini(&sequence);

    rclc_clean(node, client);
    IS_CALL_BACK = false;
}

//get parameters
void rclc_param_get_parameters(
    rcl_node_t* node,
    rcl_client_t* client,
    char* node_name,
    char* remote_name,
    char* msg_request,
    size_t size,
    char* node_name_space
)
{
    rcl_ret_t rc;
    //service type
    rcl_allocator_t allocator = rcl_get_default_allocator();
    rclc_support_t support;

    // create init_options
    rclc_support_init(&support, 0, NULL, &allocator);

    // create node
    rclc_node_init_default(node, node_name, node_name_space, &support);

    // create client
    char* service_name = create_service_name(remote_name, M_GET_PARAMETERS);
    rclc_client_init_default(
      client, node,
      rclc_param_type_support(Get_Parameters_), service_name);

    //create message
    rcl_interfaces__srv__GetParameters_Request__Sequence sequence;
    rcl_interfaces__srv__GetParameters_Response__Sequence response;
    rcl_interfaces__srv__GetParameters_Response__Sequence__init(&response, 1);
    rcl_interfaces__srv__GetParameters_Request__Sequence__init(&sequence, 1);
    rosidl_runtime_c__String__Sequence__init(&sequence.data->names, size);
    rcl_interfaces__msg__ParameterValue__Sequence__init(&response.data->values, size);

    for (size_t i=0; i < size; i++) {
        if (rosidl_runtime_c__String__init(&sequence.data->names.data[i]))
            rosidl_runtime_c__String__assign(&sequence.data->names.data[i], msg_request);
        rosidl_runtime_c__String__init(&response.data->values.data[i].string_value);
    }

    int64_t seq = 0;
    rclc_sleep_ms(1000);   // Sleep a while to ensure DDS matching before sending request

    //request count
    IS_CALL_BACK = false;
    FAIL_COUNT = 0;

    while (!IS_CALL_BACK && FAIL_COUNT < 5) {
        rc = rcl_send_request(client, sequence.data, &seq);
        rclc_send_judge(rc, service_name);
        rclc_sleep_ms(500);
        if (seq != 0 && rc == RCL_RET_OK) {
            rclc_response_message_take(client, response.data, get_parameters_callback, seq);
        }
        FAIL_COUNT++;
    }

    //fail count
    fail_count_judge();
    
    for (size_t i=0; i < size; i++) {
        rosidl_runtime_c__String__fini(&sequence.data->names.data[i]);
        rosidl_runtime_c__String__fini(&response.data->values.data[i].string_value);
    }

    rosidl_runtime_c__String__Sequence__fini(&sequence.data->names);
    rcl_interfaces__msg__ParameterValue__Sequence__fini(&response.data->values);
    rcl_interfaces__srv__GetParameters_Request__Sequence__fini(&sequence);
    rcl_interfaces__srv__GetParameters_Response__Sequence__fini(&response);

    rclc_clean(node, client);
    IS_CALL_BACK = false;
}

//get parameter_types
int* rclc_param_get_parameter_types(
    rcl_node_t* node,
    rcl_client_t* client,
    char* node_name,
    char* remote_name,
    char** msg_request,
    size_t size,
    char* node_name_space
)
{
    IS_GET = false;
    rcl_ret_t rc;
    //service type
    rcl_allocator_t allocator = rcl_get_default_allocator();
    rclc_support_t support;

    // create init_options
    rclc_support_init(&support, 0, NULL, &allocator);

    // create node
    rclc_node_init_default(node, node_name, node_name_space, &support);

    // create client
    char* service_name = create_service_name(remote_name, M_GET_PARAMETERS_TYPES);
    rclc_client_init_default(
      client, node,
      rclc_param_type_support(Get_Parameter_Types_), service_name);

    //create message
    rcl_interfaces__srv__GetParameterTypes_Request__Sequence sequence;
    rcl_interfaces__srv__GetParameterTypes_Response__Sequence response;
    rcl_interfaces__srv__GetParameterTypes_Response__Sequence__init(&response, 1);
    rcl_interfaces__srv__GetParameterTypes_Request__Sequence__init(&sequence, 1);
    rosidl_runtime_c__String__Sequence__init(&sequence.data->names, size);
    rosidl_runtime_c__uint8__Sequence__init(&response.data->types, size);

    for (size_t i=0; i < size; i++) {
        if (rosidl_runtime_c__String__init(&sequence.data->names.data[i]))
            rosidl_runtime_c__String__assign(&sequence.data->names.data[i], msg_request[i]);
    }

    int64_t seq = 0;
    rclc_sleep_ms(1000);   // Sleep a while to ensure DDS matching before sending request

    //request count
    IS_CALL_BACK = false;
    FAIL_COUNT = 0;

    while (!IS_CALL_BACK && FAIL_COUNT < 5) {
        rc = rcl_send_request(client, sequence.data, &seq);
        rclc_send_judge(rc, service_name);
        rclc_sleep_ms(500);
        if (seq != 0 && rc == RCL_RET_OK) {
            rclc_response_message_take(client, response.data, get_parameters_types_callback, seq);
        }
        FAIL_COUNT++;
    }

    //fail count
    fail_count_judge();
    
    for (size_t i=0; i < size; i++)
        rosidl_runtime_c__String__fini(&sequence.data->names.data[i]);

    rosidl_runtime_c__String__Sequence__fini(&sequence.data->names);
    rosidl_runtime_c__uint8__Sequence__fini(&response.data->types);
    rcl_interfaces__srv__GetParameterTypes_Request__Sequence__fini(&sequence);
    rcl_interfaces__srv__GetParameterTypes_Response__Sequence__fini(&response);

    rclc_clean(node, client);
    return get_Type;
}

//list parameters
void rclc_param_list_parameters(
    rcl_node_t* node,
    rcl_client_t* client,
    char* node_name,
    char* remote_name,
    char** msg_request,
    size_t size,
    char* node_name_space
)
{
    rcl_ret_t rc;
    //service type
    rcl_allocator_t allocator = rcl_get_default_allocator();
    rclc_support_t support;

    // create init_options
    rclc_support_init(&support, 0, NULL, &allocator);

    // create node
    rclc_node_init_default(node, node_name, node_name_space, &support);

    // create client
    char* service_name = create_service_name(remote_name, M_LIST_PARAMETERS);
    rclc_client_init_default(
      client, node,
      rclc_param_type_support(List_Parameters_), service_name);

    //create message
    rcl_interfaces__srv__ListParameters_Request__Sequence sequence;
    rcl_interfaces__srv__ListParameters_Response__Sequence response;
    rcl_interfaces__srv__ListParameters_Response__Sequence__init(&response, 1);
    rcl_interfaces__srv__ListParameters_Request__Sequence__init(&sequence, 1);
    rcl_interfaces__msg__ListParametersResult__Sequence__init(&response.data->result, 1);
    rosidl_runtime_c__String__Sequence__init(&sequence.data->prefixes, size);
    rosidl_runtime_c__String__Sequence__init(&response.data->result.names, size);
    rosidl_runtime_c__String__Sequence__init(&response.data->result.prefixes, size);

    for (size_t i=0; i < size; i++) {
        if (rosidl_runtime_c__String__init(&sequence.data->prefixes.data[i]))
            rosidl_runtime_c__String__assign(&sequence.data->prefixes.data[i], msg_request[i]);
    }

    int64_t seq = 0;
    rclc_sleep_ms(1000);   // Sleep a while to ensure DDS matching before sending request

    //request count
    IS_CALL_BACK = false;
    FAIL_COUNT = 0;

    while (!IS_CALL_BACK && FAIL_COUNT < 5) {
        rc = rcl_send_request(client, sequence.data, &seq);
        rclc_send_judge(rc, service_name);
        rclc_sleep_ms(500);
        if (seq != 0 && rc == RCL_RET_OK) {
            rclc_response_message_take(client, response.data, list_patameters_callback, seq);
        }
        FAIL_COUNT++;
    }

    //fail connect judge
    fail_count_judge();

    for (size_t i=0; i < size; i++)
        rosidl_runtime_c__String__fini(&sequence.data->prefixes.data[i]);

    rosidl_runtime_c__String__Sequence__fini(&sequence.data->prefixes);
    rosidl_runtime_c__String__Sequence__fini(&response.data->result.names);
    rosidl_runtime_c__String__Sequence__fini(&response.data->result.prefixes);
    rcl_interfaces__msg__ListParametersResult__Sequence__fini(&response.data->result);
    rcl_interfaces__srv__ListParameters_Response__Sequence__fini(&response);
    rcl_interfaces__srv__ListParameters_Request__Sequence__fini(&sequence);

    rclc_clean(node, client);

    IS_CALL_BACK = false;
}

//describe_parameters
void rclc_param_describe_parameters(
    rcl_node_t* node,
    rcl_client_t* client,
    char* node_name,
    char* remote_name,
    char** msg_request,
    size_t size,
    char* node_name_space
)
{
    rcl_ret_t rc;
    //service type
    rcl_allocator_t allocator = rcl_get_default_allocator();
    rclc_support_t support;

    // create init_options
    rclc_support_init(&support, 0, NULL, &allocator);

    // create node
    rclc_node_init_default(node, node_name, node_name_space, &support);

    // create client
    char* service_name = create_service_name(remote_name, M_DESCRIBE_PARAMETERS);
    rclc_client_init_default(
      client, node,
      rclc_param_type_support(Describe_Parameters_), service_name);

    //create message
    rcl_interfaces__srv__DescribeParameters_Request__Sequence sequence;
    rcl_interfaces__srv__DescribeParameters_Response__Sequence response;
    rcl_interfaces__srv__DescribeParameters_Response__Sequence__init(&response, 1);
    rcl_interfaces__srv__DescribeParameters_Request__Sequence__init(&sequence, 1);
    rosidl_runtime_c__String__Sequence__init(&sequence.data->names, size);
    rcl_interfaces__msg__ParameterDescriptor__Sequence__init(&response.data->descriptors, size);

    for (size_t i=0; i < size; i++) {
        if (rosidl_runtime_c__String__init(&sequence.data->names.data[i]))
            rosidl_runtime_c__String__assign(&sequence.data->names.data[i], msg_request[i]);
    }

    int64_t seq = 0;
    // Sleep a while to ensure DDS matching before sending request
    rclc_sleep_ms(1000);

    //request count
    IS_CALL_BACK = false;
    FAIL_COUNT = 0;

    while (!IS_CALL_BACK && FAIL_COUNT < 5) {
        rc = rcl_send_request(client, sequence.data, &seq);
        rclc_send_judge(rc, service_name);
        rclc_sleep_ms(500);
        if (seq != 0 && rc == RCL_RET_OK) {
            rclc_response_message_take(client, response.data, describe_parameters_callback, seq);
        }
        FAIL_COUNT++;
    }

    //fail count
    fail_count_judge();
    
    for (size_t i=0; i < size; i++)
        rosidl_runtime_c__String__fini(&sequence.data->names.data[i]);

    rosidl_runtime_c__String__Sequence__fini(&sequence.data->names);
    rcl_interfaces__msg__ParameterDescriptor__Sequence__fini(&response.data->descriptors);
    rcl_interfaces__srv__DescribeParameters_Response__Sequence__fini(&response);
    rcl_interfaces__srv__DescribeParameters_Request__Sequence__fini(&sequence);
    rclc_clean(node, client);
    IS_CALL_BACK = false;
}

//set parameters
bool rclc_param_set_parameters(
    rcl_node_t* node,
    rcl_client_t* client,
    char* node_name,
    char* remote_name,
    Parameter_Set_* msg_request,
    size_t size,
    char* node_name_space
)
{
    IS_SET = false;
    //get type_id
    char* str[size];
    for (size_t i = 0; i < size; i++) {
        str[i] = msg_request[i].parameter_name;
    }

    //get type_id
    rcl_node_t str_node;
    rcl_client_t str_client;
    rclc_param_init_client_node(&str_node, &str_client);
    rclc_param_get_parameter(&str_node, &str_client, "str_node", remote_name, str, size, "");

    rcl_ret_t rc;
    //service type
    rcl_allocator_t allocator = rcl_get_default_allocator();
    rclc_support_t support;

    // create init_options
    rclc_support_init(&support, 0, NULL, &allocator);

    // create node
    rclc_node_init_default(node, node_name, node_name_space, &support);

    // create client
    char* service_name = create_service_name(remote_name, M_SET_PARAMETERS);
    rclc_client_init_default(
      client, node,
      rclc_param_type_support(Set_Parameters_), service_name);

    //create message
    rcl_interfaces__srv__SetParameters_Request__Sequence sequence;
    rcl_interfaces__srv__SetParameters_Response__Sequence response;
    rcl_interfaces__srv__SetParameters_Response__Sequence__init(&response, 1);
    rcl_interfaces__srv__SetParameters_Request__Sequence__init(&sequence, 1);
    rcl_interfaces__msg__Parameter__Sequence__init(&sequence.data->parameters, size);
    rcl_interfaces__msg__SetParametersResult__Sequence__init(&response.data->results, size);
    
    for (int i = 0; i < size; i++) {
        rosidl_runtime_c__String__init(&response.data->results.data[i].reason);
    }
    //init msg
    for (size_t i = 0; i < size; i++) {
        rosidl_runtime_c__String__init(&sequence.data->parameters.data[i].name);
        rosidl_runtime_c__String__assign(&sequence.data->parameters.data[i].name, msg_request[i].parameter_name);
        rcl_interfaces__msg__ParameterValue__init(&sequence.data->parameters.data[i].value);
        bool type_Judge = true;
        switch (type_Id[i])
        {
        case Parameter_BOOL:
            sequence.data->parameters.data[i].value.bool_value = msg_request[i].data.bool_value;
            break;
        case Parameter_INT:
            if (msg_request[i].data.interger_value != DEFAULT_INT)
                sequence.data->parameters.data[i].value.integer_value = msg_request[i].data.interger_value;
            break;
        case Parameter_DOUBLE:
            if (!(fabs(msg_request[i].data.double_value - DEFAULT_DOUBLE) < DEFAULT_DOUBLE))        
                sequence.data->parameters.data[i].value.double_value = msg_request[i].data.double_value;
            break;
        case Parameter_STRING:
            if (msg_request[i].data.string_value != DEFAULT_STRING)
                if (rosidl_runtime_c__String__init(&sequence.data->parameters.data[i].value.string_value))
                    rosidl_runtime_c__String__assign(&sequence.data->parameters.data[i].value.string_value, msg_request[i].data.string_value);
            break;
        default:
            type_Judge = false;
            break;
        }
        if (type_Judge) sequence.data->parameters.data[i].value.type = type_Id[i];
    }

    int64_t seq = 0;
    // Sleep a while to ensure DDS matching before sending request
    rclc_sleep_ms(1000);

    //request count
    IS_CALL_BACK = false;
    FAIL_COUNT = 0;

    while (!IS_CALL_BACK && FAIL_COUNT < 5) {
        rc = rcl_send_request(client, sequence.data, &seq);
        rclc_send_judge(rc, service_name);
        rclc_sleep_ms(500);
        if (seq != 0 && rc == RCL_RET_OK) {
            rclc_response_message_take(client, response.data, set_parameters_callback, seq);
        }
        FAIL_COUNT++;
    }

    //fail count
    fail_count_judge();
    
    //clearn up
    for (size_t i = 0; i < size; i++) {
        if (type_Id[i] == Parameter_STRING)
            rcl_interfaces__msg__ParameterValue__fini(&sequence.data->parameters.data[i].value);
        rosidl_runtime_c__String__fini(&response.data->results.data[i].reason);
    }

    rcl_interfaces__msg__Parameter__Sequence__fini(&sequence.data->parameters);
    rcl_interfaces__msg__SetParametersResult__Sequence__fini(&response.data->results);
    rcl_interfaces__srv__SetParameters_Response__Sequence__fini(&response);
    rcl_interfaces__srv__SetParameters_Request__Sequence__fini(&sequence);
    rclc_clean(node, client);

    IS_CALL_BACK = false;
    return IS_SET;
}

//set_parameters_atomically
bool rclc_param_set_parameters_atomically(
    rcl_node_t* node,
    rcl_client_t* client,
    char* node_name,
    char* remote_name,
    Parameter_Set_* msg_request,
    size_t size,
    char* node_name_space
)
{
    IS_SET = false;
    //get type_id
    char* str[size];
    for (size_t i = 0; i < size; i++) {
        str[i] = msg_request[i].parameter_name;
    }

    //get type_id
    rcl_node_t str_node;
    rcl_client_t str_client;
    rclc_param_init_client_node(&str_node, &str_client);
    rclc_param_get_parameter(&str_node, &str_client, "str_node", remote_name, str, size, "");

    rcl_ret_t rc;
    //service type
    rcl_allocator_t allocator = rcl_get_default_allocator();
    rclc_support_t support;

    // create init_options
    rclc_support_init(&support, 0, NULL, &allocator);

    // create node
    rclc_node_init_default(node, node_name, node_name_space, &support);

    // create client
    char* service_name = create_service_name(remote_name, M_SET_PARAMETERS_ATOMICALLY);
    rclc_client_init_default(
      client, node,
      rclc_param_type_support(Set_Parameters_Atomically_), service_name);

    //create message
    rcl_interfaces__srv__SetParametersAtomically_Request__Sequence sequence;
    rcl_interfaces__srv__SetParametersAtomically_Response__Sequence response;
    rcl_interfaces__srv__SetParametersAtomically_Response__Sequence__init(&response, 1);
    rcl_interfaces__srv__SetParametersAtomically_Request__Sequence__init(&sequence, 1);
    rcl_interfaces__msg__Parameter__Sequence__init(&sequence.data->parameters, size);
    rosidl_runtime_c__String__init(&response.data->result.reason);

    //init msg
    for (size_t i = 0; i < size; i++) {
        rosidl_runtime_c__String__init(&sequence.data->parameters.data[i].name);
        rosidl_runtime_c__String__assign(&sequence.data->parameters.data[i].name, msg_request[i].parameter_name);
        rcl_interfaces__msg__ParameterValue__init(&sequence.data->parameters.data[i].value);
        bool type_Judge = true;
        switch (type_Id[i])
        {
        case Parameter_BOOL:
            sequence.data->parameters.data[i].value.bool_value = msg_request[i].data.bool_value;
            break;
        case Parameter_INT:
            sequence.data->parameters.data[i].value.integer_value = msg_request[i].data.interger_value;
            break;
        case Parameter_DOUBLE:
            sequence.data->parameters.data[i].value.double_value = msg_request[i].data.double_value;
            break;
        case Parameter_STRING:
            if (rosidl_runtime_c__String__init(&sequence.data->parameters.data[i].value.string_value))
                rosidl_runtime_c__String__assign(&sequence.data->parameters.data[i].value.string_value, msg_request[i].data.string_value);
            break;
        default:
            type_Judge = false;
            break;
        }
        if (type_Judge) sequence.data->parameters.data[i].value.type = type_Id[i];
    }

    int64_t seq = 0;
    // Sleep a while to ensure DDS matching before sending request
    rclc_sleep_ms(1000);

    //request count
    IS_CALL_BACK = false;
    FAIL_COUNT = 0;

    while (!IS_CALL_BACK && FAIL_COUNT < 5) {
        rc = rcl_send_request(client, sequence.data, &seq);
        rclc_send_judge(rc, service_name);
        rclc_sleep_ms(500);
        if (seq != 0 && rc == RCL_RET_OK) {
            rclc_response_message_take(client, response.data, set_parameters_atomically_callback, seq);
        }
        FAIL_COUNT++;
    }

    //fail count
    fail_count_judge();
    
    //clearn up
    for (size_t i = 0; i < size; i++) {
        if (type_Id[i] == Parameter_STRING)
            rcl_interfaces__msg__ParameterValue__fini(&sequence.data->parameters.data[i].value);
    }

    rosidl_runtime_c__String__fini(&response.data->result.reason);
    rcl_interfaces__msg__Parameter__Sequence__fini(&sequence.data->parameters);
    rcl_interfaces__srv__SetParametersAtomically_Response__Sequence__fini(&response);
    rcl_interfaces__srv__SetParametersAtomically_Request__Sequence__fini(&sequence);
    rclc_clean(node, client);

    IS_CALL_BACK = false;
    return IS_SET;
}

//set parameter for single
bool rclc_param_set_parameter_for_single(
    rcl_node_t* node,
    rcl_client_t* client,
    char* node_name,
    char* remote_name,
    char* parameter_name,
    int parameter_Type,
    char* node_name_space
)
{
    IS_SET = false;
    //get type_id
    char* str[1];
    str[0] = parameter_name;

    //get type_id
    rcl_node_t str_node;
    rcl_client_t str_client;
    rclc_param_init_client_node(&str_node, &str_client);
    rclc_param_get_parameter(&str_node, &str_client, "str_node", remote_name, str, 1, "");

    rcl_ret_t rc;
    //service type
    rcl_allocator_t allocator = rcl_get_default_allocator();
    rclc_support_t support;

    // create init_options
    rclc_support_init(&support, 0, NULL, &allocator);

    // create node
    rclc_node_init_default(node, node_name, node_name_space, &support);

    // create client
    char* service_name = create_service_name(remote_name, M_SET_PARAMETERS);
    rclc_client_init_default(
      client, node,
      rclc_param_type_support(Set_Parameters_), service_name);

    //create message
    rcl_interfaces__srv__SetParameters_Request__Sequence sequence;
    rcl_interfaces__srv__SetParameters_Response__Sequence response;
    rcl_interfaces__srv__SetParameters_Response__Sequence__init(&response, 1);
    rcl_interfaces__srv__SetParameters_Request__Sequence__init(&sequence, 1);
    rcl_interfaces__msg__SetParametersResult__Sequence__init(&response.data->results, 1);
    rosidl_runtime_c__String__init(&response.data->results.data->reason);
    
    //init msg
    bool type_Judge = true;
    if (type_Id[0] == parameter_Type) {
        rcl_interfaces__msg__Parameter__Sequence__init(&sequence.data->parameters, 1);
        rosidl_runtime_c__String__init(&sequence.data->parameters.data->name);
        rosidl_runtime_c__String__assign(&sequence.data->parameters.data->name, parameter_name);
        rcl_interfaces__msg__ParameterValue__init(&sequence.data->parameters.data->value);
        switch (type_Id[0]) 
        {
        case Parameter_BOOL:
            sequence.data->parameters.data->value.bool_value = set_Data.data.bool_value;
            break;
        case Parameter_INT:
            sequence.data->parameters.data->value.integer_value = set_Data.data.interger_value;
            break;
        case Parameter_DOUBLE:        
            sequence.data->parameters.data->value.double_value = set_Data.data.double_value;
            break;
        case Parameter_STRING:
            if (rosidl_runtime_c__String__init(&sequence.data->parameters.data->value.string_value))
                rosidl_runtime_c__String__assign(&sequence.data->parameters.data->value.string_value, set_Data.data.string_value);
            break;
        default:
            type_Judge = false;
            break;
        }
        if (type_Judge) sequence.data->parameters.data->value.type = type_Id[0];
    }

    int64_t seq = 0;
    // Sleep a while to ensure DDS matching before sending request
    rclc_sleep_ms(1000);

    //request count
    IS_CALL_BACK = false;
    FAIL_COUNT = 0;

    while (!IS_CALL_BACK && FAIL_COUNT < 5) {
        rc = rcl_send_request(client, sequence.data, &seq);
        rclc_send_judge(rc, service_name);
        rclc_sleep_ms(500);
        if (seq != 0 && rc == RCL_RET_OK) {
            rclc_response_message_take(client, response.data, set_parameters_callback, seq);
        }
        FAIL_COUNT++;
    }

    //fail count
    fail_count_judge();
    
    //clearn up
    if (type_Id[0] == parameter_Type) 
        rcl_interfaces__msg__ParameterValue__fini(&sequence.data->parameters.data->value);

    rcl_interfaces__msg__Parameter__Sequence__fini(&sequence.data->parameters);
    rcl_interfaces__msg__SetParametersResult__Sequence__fini(&response.data->results);
    rcl_interfaces__srv__SetParameters_Response__Sequence__fini(&response);
    rcl_interfaces__srv__SetParameters_Request__Sequence__fini(&sequence);
    rclc_clean(node, client);

    IS_CALL_BACK = false;
    return IS_SET;
}

//get_parameter_int
int64_t rclc_param_get_parameter_int(
    rcl_node_t* node,
    rcl_client_t* client,
    char* node_name,
    char* remote_name,
    char* msg_request,
    char* node_name_space
)
{
    rclc_param_get_parameters(node, client, node_name, remote_name, msg_request, 1, node_name_space);
    if (IS_GET) {
        return get_Data.interger_value;
    }
    printf("parameter is not exist!\n");
    return -1;
}

//get_parameter_double
double rclc_param_get_parameter_double(
    rcl_node_t* node,
    rcl_client_t* client,
    char* node_name,
    char* remote_name,
    char* msg_request,
    char* node_name_space
)
{
    rclc_param_get_parameters(node, client, node_name, remote_name, msg_request, 1, node_name_space);
    if (IS_GET) {
        return get_Data.double_value;
    }
    printf("parameter is not exist!\n");
    return -1;
}

//get_parameter_string
char* rclc_param_get_parameter_string(
    rcl_node_t* node,
    rcl_client_t* client,
    char* node_name,
    char* remote_name,
    char* msg_request,
    char* node_name_space
)
{
    rclc_param_get_parameters(node, client, node_name, remote_name, msg_request, 1, node_name_space);
    if (IS_GET) {
        return get_Data.string_value;
    }
    printf("parameter is not exist!\n");
    return "";
}

//get_parameter_bool
bool rclc_param_get_parameter_bool(
    rcl_node_t* node,
    rcl_client_t* client,
    char* node_name,
    char* remote_name,
    char* msg_request,
    char* node_name_space
)
{
    rclc_param_get_parameters(node, client, node_name, remote_name, msg_request, 1, node_name_space);
    if (IS_GET) {
        return get_Data.bool_value;
    }
    printf("parameter is not exist!\n");
    return false;
}

//parameter set of int
bool rclc_param_set_parameters_int(
    rcl_node_t* node,
    rcl_client_t* client,
    char* node_name,
    char* remote_name,
    char* parameter_name,
    int64_t parameter_value,
    char* node_name_space
)
{
    if (parameter_value >= DEFAULT_INT) {
        set_Data.data.interger_value = parameter_value;
        return rclc_param_set_parameter_for_single(node, client, node_name, remote_name, parameter_name, Parameter_INT, node_name_space);
    }
    return false;
}

//parameter set of double
bool rclc_param_set_parameters_double(
    rcl_node_t* node,
    rcl_client_t* client,
    char* node_name,
    char* remote_name,
    char* parameter_name,
    double parameter_value,
    char* node_name_space
)
{
    if (!(fabs(parameter_value - DEFAULT_DOUBLE) < DEFAULT_DOUBLE)) {
        set_Data.data.double_value = parameter_value;
        return rclc_param_set_parameter_for_single(node, client, node_name, remote_name, parameter_name, Parameter_DOUBLE, node_name_space);
    }
    return false;
}

//parameter set of string
bool rclc_param_set_parameters_string(
    rcl_node_t* node,
    rcl_client_t* client,
    char* node_name,
    char* remote_name,
    char* parameter_name,
    char* parameter_value,
    char* node_name_space
)
{
    if (parameter_value != DEFAULT_STRING) {
        set_Data.data.string_value = parameter_value;
        return rclc_param_set_parameter_for_single(node, client, node_name, remote_name, parameter_name, Parameter_STRING, node_name_space);
    }
}

//parameter set of bool
bool rclc_param_set_parameters_bool(
    rcl_node_t* node,
    rcl_client_t* client,
    char* node_name,
    char* remote_name,
    char* parameter_name,
    bool parameter_value,
    char* node_name_space
)
{
    set_Data.data.bool_value = parameter_value;
    return rclc_param_set_parameter_for_single(node, client, node_name, remote_name, parameter_name, Parameter_BOOL, node_name_space);
}

void parameter_callback(const void* msg)
{
    if (dynamic_Size == 0) return;
    rcl_interfaces__msg__ParameterEvent* res = (rcl_interfaces__msg__ParameterEvent*)msg;
    if (!res->changed_parameters.size) return;
    for (size_t i = 0; i < dynamic_Size; i++) {
        if (strcmp(res->node.data, dynamic_Data[i].node_name) == 0 && strcmp(dynamic_Data[i].parameter_name, res->changed_parameters.data->name.data) == 0){
            switch (res->changed_parameters.data->value.type) 
            {
            case Parameter_BOOL:
                dynamic_Data[i].data.bool_value = res->changed_parameters.data->value.bool_value;
                break;
            case Parameter_INT:
                dynamic_Data[i].data.interger_value = res->changed_parameters.data->value.integer_value;
                break;
            case Parameter_DOUBLE:        
                dynamic_Data[i].data.double_value = res->changed_parameters.data->value.double_value;
                break;
            case Parameter_STRING:
                dynamic_Data[i].data.string_value = res->changed_parameters.data->value.string_value.data;
                break;
            default:
                break;
            }
            
        }
    }
}

void* thread_deal_parameter_event(void* arg)
{
    rclc_support_t support;

    rcl_allocator_t allocator = rcl_get_default_allocator();
    // create init_options
    rclc_support_init(&support, 0, NULL, &allocator);
    rclc_node_init_default(&parameter_Change_Node, "pthread_test", "", &support);
    const rosidl_message_type_support_t * type_support =
    ROSIDL_GET_MSG_TYPE_SUPPORT(rcl_interfaces, msg, ParameterEvent);
    parameter_Change_Subscription = rcl_get_zero_initialized_subscription();
    
    //set qos
    rclc_subscription_init(&parameter_Change_Subscription, &parameter_Change_Node, type_support, "/parameter_events", &rcl_msg_qos_profile_status_default);

    unsigned int rcl_wait_timeout = 10;         // in ms
    parameter_Change_Executor = rclc_executor_get_zero_initialized_executor();
    rclc_executor_init(&parameter_Change_Executor, &support.context, 1, &allocator);
    rclc_executor_set_timeout(&parameter_Change_Executor, RCL_MS_TO_NS(rcl_wait_timeout));
    rcl_interfaces__msg__ParameterEvent__Sequence Response;
    rcl_interfaces__msg__ParameterEvent__Sequence__init(&Response, 1);
    rosidl_runtime_c__String__init(&Response.data->node);
    rcl_interfaces__msg__Parameter__Sequence__init(&Response.data->new_parameters, dynamic_Size);
    rcl_interfaces__msg__Parameter__Sequence__init(&Response.data->changed_parameters, dynamic_Size);
    rcl_interfaces__msg__Parameter__Sequence__init(&Response.data->deleted_parameters, dynamic_Size);
    rclc_executor_add_subscription(&parameter_Change_Executor, &parameter_Change_Subscription, Response.data, parameter_callback, 0);
    rclc_executor_spin(&parameter_Change_Executor);
}

void thread_param_init()
{
    int pthread_start = pthread_create(&parameter_Change_Thread, NULL, thread_deal_parameter_event, NULL);
    if (pthread_start != 0) printf("pthread create error!\n");
    else parameter_init = true;
}

//callback
void rclc_on_parameter_event(
    Parameter_Set_* msg_request,
    size_t size
)
{
    if (size == 0) return;
    dynamic_Size = size;
    dynamic_Data = msg_request;

    if (!parameter_init) {
        thread_param_init();
        pthread_detach(parameter_Change_Thread);
    }
}
