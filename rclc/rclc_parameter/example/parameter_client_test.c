#include "rclc_parameter/parameter_client.h"


int main(int argc, char** argv)
{
    rcl_node_t node;
    rcl_client_t client;

    rclc_param_init_client_node(&node, &client);

    char* remote_name = "/test_node";

    //int64_t test
    int64_t m_int = rclc_param_get_parameter_int(&node, &client, "tttt", remote_name, "test_int", "");
    printf("get int value: %d\n", m_int);

    bool ret = rclc_param_set_parameters_int(&node, &client, "tttt", remote_name, "test_int",  22, "");
    printf("set int success: %d\n", ret);

    m_int = rclc_param_get_parameter_int(&node, &client, "tttt", remote_name, "test_int", "");
    printf("get int value: %d\n", m_int == 22);



    //bool test
    bool m_bool = rclc_param_get_parameter_bool(&node, &client, "tttt", remote_name, "test_bool", "");
    printf("get bool value: %d\n", m_bool);

    m_bool = rclc_param_set_parameters_bool(&node, &client, "tttt", remote_name, "test_bool",  true, "");
    printf("set bool success: %d\n", m_bool);

    m_bool = rclc_param_get_parameter_bool(&node, &client, "tttt", remote_name, "test_bool", "");
    printf("get bool value: %d\n", m_bool == true);



    //string test
    char* m_string = rclc_param_get_parameter_string(&node, &client, "tttt", remote_name, "test_string", "");
    printf("get string value: %s\n", m_string);

    bool m_stringbool = rclc_param_set_parameters_string(&node, &client, "tttt", remote_name, "test_string",  "pppppppppp", "");
    printf("set string success: %d\n", m_stringbool);

    m_string = rclc_param_get_parameter_string(&node, &client, "tttt", remote_name, "test_string", "");
    printf("get string value: %s\n", m_string);



    //double test
    double m_double = rclc_param_get_parameter_double(&node, &client, "tttt", remote_name, "test_double", "");
    printf("get double value: %f\n", m_double);

    bool m_doublebool = rclc_param_set_parameters_double(&node, &client, "tttt", remote_name, "test_double",  99.99, "");
    printf("set double success: %d\n", m_doublebool);

    m_int = rclc_param_get_parameter_double(&node, &client, "tttt", remote_name, "test_double", "");
    printf("get double value: %f\n", m_double);

    return 0;
}