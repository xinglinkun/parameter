#if __cplusplus
extern "C"
{
#endif  // if __cplusplus
#include <math.h>
#include <pthread.h>

#include <rcl/rcl.h>
#include <rcl/error_handling.h>
#include <rclc/rclc.h>
#include <rclc/executor.h>
#include <rcl/types.h>
#include <rmw/types.h>

#include <rcl_interfaces/msg/parameter.h>
#include <rcl_interfaces/msg/parameter_value.h>
#include <rcl_interfaces/msg/parameter_event.h>
#include <rcl_interfaces/msg/parameter_descriptor.h>
#include <rcl_interfaces/msg/list_parameters_result.h>
#include <rcl_interfaces/msg/set_parameters_result.h>

#include <rcl_interfaces/srv/get_parameter_types.h>
#include <rcl_interfaces/srv/get_parameters.h>
#include <rcl_interfaces/srv/list_parameters.h>
#include <rcl_interfaces/srv/set_parameters.h>
#include <rcl_interfaces/srv/describe_parameters.h>
#include <rcl_interfaces/srv/set_parameters_atomically.h>
#include <rosidl_runtime_c/string_functions.h>
#include <rosidl_runtime_c/primitives_sequence_functions.h>
#include <rclc_parameter/visibility_control.h>
#define DEFAULT_STRING "RCLC_TEST_FOR_CLIENT_YES_OR_NO_"
#define DEFAULT_DOUBLE 1e-6
#define DEFAULT_INT INT64_MIN

/**
    *data: Basic_Data_Set_ struct: it include int64_t, double, string, bool and more types of pameter
*/
typedef struct Basic_Data_Set_
{
    bool bool_value;
    int64_t interger_value;
    double double_value;
    char* string_value;
    uint8_t* byte_array_value;
    bool* bool_array_value;
    int64_t* interger_array_value;
    double* double_array_value;
    char** string_array_value;  
}Basic_Data_Set_;


/**
    *struct of set parameters
    *parameter_name: the name of parameter
    *node_name: node name of parameter
    *data: Basic_Data_Set_ struct, include int64_t, double, string, bool and more types of pameter
*/
typedef struct Parameter_Set_
{
    char* parameter_name;
    char* node_name;
    Basic_Data_Set_ data;
}Parameter_Set_;


//init node and client
void rclc_param_init_client_node(
    rcl_node_t* node,
    rcl_client_t* client
);

//get_parameter_int
int64_t rclc_param_get_parameter_int(
    rcl_node_t* node,
    rcl_client_t* client,
    char* node_name,
    char* remote_name,
    char* parameter_name,
    char* node_name_space
);

//get_parameter_double
double rclc_param_get_parameter_double(
    rcl_node_t* node,
    rcl_client_t* client,
    char* node_name,
    char* remote_name,
    char* parameter_name,
    char* node_name_space
);

//get_parameter_string
char* rclc_param_get_parameter_string(
    rcl_node_t* node,
    rcl_client_t* client,
    char* node_name,
    char* remote_name,
    char* parameter_name,
    char* node_name_space
);

//get_parameter_bool
bool rclc_param_get_parameter_bool(
    rcl_node_t* node,
    rcl_client_t* client,
    char* node_name,
    char* remote_name,
    char* parameter_name,
    char* node_name_space
);

//get_parameter_types
int* rclc_param_get_parameter_types(
    rcl_node_t* node,
    rcl_client_t* client,
    char* node_name,
    char* remote_name,
    char** msg_request,
    size_t size,
    char* node_name_space
);

//list_parameters
void rclc_param_list_parameters(
    rcl_node_t* node,
    rcl_client_t* client,
    char* node_name,
    char* remote_name,
    char** msg_request,
    size_t size,
    char* node_name_space
);

//describe parameters
void rclc_param_describe_parameters(
    rcl_node_t* node,
    rcl_client_t* client,
    char* node_name,
    char* remote_name,
    char** msg_request,
    size_t size,
    char* node_name_space
);

//set_parameters
bool rclc_param_set_parameters(
    rcl_node_t* node,
    rcl_client_t* client,
    char* node_name,
    char* remote_name,
    Parameter_Set_* msg_request,
    size_t size,
    char* node_name_space
);

//set_parameters_int
bool rclc_param_set_parameters_int(
    rcl_node_t* node,
    rcl_client_t* client,
    char* node_name,
    char* remote_name,
    char* parameter_name,
    int64_t parameter_value,
    char* node_name_space
);

//set_parameters_double
bool rclc_param_set_parameters_double(
    rcl_node_t* node,
    rcl_client_t* client,
    char* node_name,
    char* remote_name,
    char* parameter_name,
    double parameter_value,
    char* node_name_space
);

//set_parameters_string
bool rclc_param_set_parameters_string(
    rcl_node_t* node,
    rcl_client_t* client,
    char* node_name,
    char* remote_name,
    char* parameter_name,
    char* parameter_value,
    char* node_name_space
);

//set_parameters_bool
bool rclc_param_set_parameters_bool(
    rcl_node_t* node,
    rcl_client_t* client,
    char* node_name,
    char* remote_name,
    char* parameter_name,
    bool parameter_value,
    char* node_name_space
);


//set_parameters_atomically
bool rclc_param_set_parameters_atomically(
    rcl_node_t* node,
    rcl_client_t* client,
    char* node_name,
    char* remote_name,
    Parameter_Set_* msg_request,
    size_t size,
    char* node_name_space
);

/**
    *this function that you can listen parameter change
    *parameter_Set_:  this is input/output paramer struct
    *parameter_name: the name of parameter, input parameter
    *node_name: node name of parameter, should add '/' in the head， input parameter
    *data: Basic_Data_Set_ struct, include int64_t, double, string, bool and more types of pameter， output paramter
    *size： must equal to Parameter_Set_ size
*/
void rclc_on_parameter_event(
    Parameter_Set_* msg_request,
    size_t size
);
#if __cplusplus
}
#endif