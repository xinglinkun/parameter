# Parameter client 总结

#### 一. 	查看rclcpp  paramserver 与 paramclient源码，梳理需要写的接口

##### 1.get_parameters    ----------------> 传入参数名，获取参数值（数据结构保存在rcl_interfaces中）

##### 2.get_parameter_types    ----------------> 传入参数名，获取参数类型type（数据结构保存在rcl_interfaces中）

##### 3.set_parameters  ----------------> 传入参数名参数值，返回是否设置成功（数据结构保存在rcl_interfaces中）

##### 4.set_parameters_atomically    ----------------> 传入参数名参数值，返回是否设置成功（数据结构保存在rcl_interfaces中）

##### 5.list_parameters   ----------------> 获取参数信息（参数名，前缀）（数据结构保存在rcl_interfaces中）

##### 6.describe_parameters   ----------------> 传入参数名，获取参数信息（参数名，类型，描述，是否可读，类型是否可变等）（数据结构保存在rcl_interfaces中）

##### 7.on_parameter_event    ----------------> 参数回调，能够更新来自其他节点参数的变更，不用轮询去获取



以上接口大致流程如下：

```mermaid
graph TD;
	S[程序开始] --> A[调用API]
    A --> B[创建客户端]
    B --> C[注册回调函数]
    B --> D[初始化数据]
    D --> E{数据发送}
    C --> F[回调函数实现]
    E --> |发送成功| F
    E --> |发送失败| G[程序结束]
    F --> G
```

#### 二.	rclc下的parameter_client接口设计及实现

##### 1.rclc_param_get_parameter_int

参数描述

|     参数名      |     类型      | 是否必须 |                  描述                  |
| :-------------: | :-----------: | :------: | :------------------------------------: |
|      node       |  rcl_node_t*  |   必须   |         传入参数，传入一个节点         |
|     client      | rcl_client_t* |   必须   |        传入参数，传入一个client        |
|    node_name    |     char*     |   必须   |          传入参数，传入节点名          |
|   remote_name   |     char*     |   必须   |        传入参数，service节点名         |
| parameter_name  |     char*     |   必须   |            传入参数，参数名            |
| node_name_space |     char*     |   必须   | 传入参数，node参数的命名空间，可以为空 |

返回值描述

|     返回值     |  类型   |     描述     |
| :------------: | :-----: | :----------: |
| return int64_t | int64_t | 返回节点的值 |

接口描述

此接口主要为获取int64_t参数的值，传参如上述参数描述



##### 2.rclc_param_get_parameter_double

参数描述

|     参数名      |     类型      | 是否必须 |                  描述                  |
| :-------------: | :-----------: | :------: | :------------------------------------: |
|      node       |  rcl_node_t*  |   必须   |         传入参数，传入一个节点         |
|     client      | rcl_client_t* |   必须   |        传入参数，传入一个client        |
|    node_name    |     char*     |   必须   |          传入参数，传入节点名          |
|   remote_name   |     char*     |   必须   |        传入参数，service节点名         |
| parameter_name  |     char*     |   必须   |            传入参数，参数名            |
| node_name_space |     char*     |   必须   | 传入参数，node参数的命名空间，可以为空 |

返回值描述

|    返回值     |  类型  |     描述     |
| :-----------: | :----: | :----------: |
| return double | double | 返回节点的值 |

接口描述

此接口主要为获取double参数的值，传参如上述参数描述



##### 3.rclc_param_get_parameter_string

参数描述

|     参数名      |     类型      | 是否必须 |                  描述                  |
| :-------------: | :-----------: | :------: | :------------------------------------: |
|      node       |  rcl_node_t*  |   必须   |         传入参数，传入一个节点         |
|     client      | rcl_client_t* |   必须   |        传入参数，传入一个client        |
|    node_name    |     char*     |   必须   |          传入参数，传入节点名          |
|   remote_name   |     char*     |   必须   |        传入参数，service节点名         |
| parameter_name  |     char*     |   必须   |            传入参数，参数名            |
| node_name_space |     char*     |   必须   | 传入参数，node参数的命名空间，可以为空 |

返回值描述

|    返回值    | 类型  |     描述     |
| :----------: | :---: | :----------: |
| return char* | char* | 返回节点的值 |

接口描述

此接口主要为获取char*参数的值，传参如上述参数描述



##### 4.rclc_param_get_parameter_bool

参数描述

|     参数名      |     类型      | 是否必须 |                  描述                  |
| :-------------: | :-----------: | :------: | :------------------------------------: |
|      node       |  rcl_node_t*  |   必须   |         传入参数，传入一个节点         |
|     client      | rcl_client_t* |   必须   |        传入参数，传入一个client        |
|    node_name    |     char*     |   必须   |          传入参数，传入节点名          |
|   remote_name   |     char*     |   必须   |        传入参数，service节点名         |
| parameter_name  |     char*     |   必须   |            传入参数，参数名            |
| node_name_space |     char*     |   必须   | 传入参数，node参数的命名空间，可以为空 |

返回值描述

|   返回值    | 类型 |     描述     |
| :---------: | :--: | :----------: |
| return bool | bool | 返回节点的值 |

接口描述

此接口主要为获取bool参数的值，传参如上述参数描述



##### 5.rclc_param_get_parameter_types

参数描述

|     参数名      |     类型      | 是否必须 |                             描述                             |
| :-------------: | :-----------: | :------: | :----------------------------------------------------------: |
|      node       |  rcl_node_t*  |   必须   |                    传入参数，传入一个节点                    |
|     client      | rcl_client_t* |   必须   |                   传入参数，传入一个client                   |
|    node_name    |     char*     |   必须   |                     传入参数，传入节点名                     |
|   remote_name   |     char*     |   必须   |                   传入参数，service节点名                    |
|   msg_request   |    char**     |   必须   |               传入参数，需要查询type的参数数组               |
|      size       |    size_t     |   必须   | 传入参数，msg_request数组大小，必须小于等于msg_request的size |
| node_name_space |     char*     |   必须   |            传入参数，node参数的命名空间，可以为空            |

返回值描述

|   返回值    | 类型 |        描述        |
| :---------: | :--: | :----------------: |
| return int* | int* | 返回各个参数的type |

接口描述

此接口主要为获取参数的类型id，{

​	无效参数返回0

​	bool类型返回1

​	int类型返回2

​	double类型返回3

​	char*类型返回4

}



##### 6.rclc_param_list_parameters

参数描述

|     参数名      |     类型      | 是否必须 |                             描述                             |
| :-------------: | :-----------: | :------: | :----------------------------------------------------------: |
|      node       |  rcl_node_t*  |   必须   |                    传入参数，传入一个节点                    |
|     client      | rcl_client_t* |   必须   |                   传入参数，传入一个client                   |
|    node_name    |     char*     |   必须   |                     传入参数，传入节点名                     |
|   remote_name   |     char*     |   必须   |                   传入参数，service节点名                    |
|   msg_request   |    char**     |   必须   |               传入参数，需要打印参数信息的数组               |
|      size       |    size_t     |   必须   | 传入参数，msg_request数组大小，必须小于等于msg_request的size |
| node_name_space |     char*     |   必须   |            传入参数，node参数的命名空间，可以为空            |

返回值描述

|   返回值    | 类型 |   描述   |
| :---------: | :--: | :------: |
| return void | void | 无返回值 |

接口描述

该接口主要是打印出传入参数的信息



##### 7.rclc_param_describe_parameters

参数描述

|     参数名      |     类型      | 是否必须 |                             描述                             |
| :-------------: | :-----------: | :------: | :----------------------------------------------------------: |
|      node       |  rcl_node_t*  |   必须   |                    传入参数，传入一个节点                    |
|     client      | rcl_client_t* |   必须   |                   传入参数，传入一个client                   |
|    node_name    |     char*     |   必须   |                     传入参数，传入节点名                     |
|   remote_name   |     char*     |   必须   |                   传入参数，service节点名                    |
|   msg_request   |    char**     |   必须   |                传入参数，需要打印其参数的数组                |
|      size       |    size_t     |   必须   | 传入参数，msg_request数组大小，必须小于等于msg_request的size |
| node_name_space |     char*     |   必须   |            传入参数，node参数的命名空间，可以为空            |

返回值描述

|   返回值    | 类型 |   描述   |
| :---------: | :--: | :------: |
| return void | void | 无返回值 |

接口描述

该接口主要也是打印参数信息，包括是否为只读，能否动态更改类型等，后面有需要会返回有用的信息



##### 8.rclc_param_set_parameters

参数描述

|     参数名      |      类型       | 是否必须 |                             描述                             |
| :-------------: | :-------------: | :------: | :----------------------------------------------------------: |
|      node       |   rcl_node_t*   |   必须   |                    传入参数，传入一个节点                    |
|     client      |  rcl_client_t*  |   必须   |                   传入参数，传入一个client                   |
|    node_name    |      char*      |   必须   |                     传入参数，传入节点名                     |
|   remote_name   |      char*      |   必须   |                   传入参数，service节点名                    |
|   msg_request   | Parameter_Set_* |   必须   |               传入参数，传递需要修改的参数与值               |
|      size       |     size_t      |   必须   | 传入参数，msg_request数组大小，必须小于等于msg_request的size |
| node_name_space |      char*      |   必须   |            传入参数，node参数的命名空间，可以为空            |

返回值描述

|   返回值    | 类型 |          描述          |
| :---------: | :--: | :--------------------: |
| return bool | bool | 返回设置参数值是否成功 |



Parameter_Set_ 描述

|     参数名      |  类型  | 是否必须 |          描述          |
| :-------------: | :----: | :------: | :--------------------: |
| parameter_name  | char*  |    是    |         参数名         |
|    node_name    | char*  |    否    |         节点名         |
| Basic_Data_Set_ | 结构体 |    是    | 参数值结构体，如下所诉 |



Basic_Data_Set_描述

|        参数名        |   类型   | 是否必须 |             描述             |
| :------------------: | :------: | :------: | :--------------------------: |
|      bool_value      |   bool   |    否    |  传入参数，参数bool类型的值  |
|    interger_value    | int64_t  |    否    |  传入参数，参数int类型的值   |
|     double_value     |  double  |    否    | 传入参数，参数double类型的值 |
|     string_value     |  char*   |    否    | 传入参数，参数char*类型的值  |
|   byte_array_value   | uint8_t* |    否    |          暂时未使用          |
|   bool_array_value   |  bool*   |    否    |          暂时未使用          |
| interger_array_value | int64_t* |    否    |          暂时未使用          |
|  double_array_value  | double*  |    否    |          暂时未使用          |
|  string_array_value  |  char**  |    否    |          暂时未使用          |

Basic_Data_Set_结构体需与参数类型一直进行设置方可有效，例如 参数类型为double， 此时在Parameter_Set_  中的Basic_Data_Set_设置double_value才能够如期望进行修改，否则不能修改

接口描述

此接口用于设置多个参数值的接口，但设置时需要使用者知道参数类型并进行参数设置方可有效，相信你修改参数你是知道参数类型的，否则会导致修改不成功！



##### 9.rclc_param_set_parameters_int

参数描述

|     参数名      |     类型      | 是否必须 |                  描述                  |
| :-------------: | :-----------: | :------: | :------------------------------------: |
|      node       |  rcl_node_t*  |   必须   |         传入参数，传入一个节点         |
|     client      | rcl_client_t* |   必须   |        传入参数，传入一个client        |
|    node_name    |     char*     |   必须   |          传入参数，传入节点名          |
|   remote_name   |     char*     |   必须   |        传入参数，service节点名         |
| parameter_name  |     char*     |   必须   |            传入参数，参数名            |
| parameter_value |    int64_t    |   必须   |            传入参数，参数值            |
| node_name_space |     char*     |   必须   | 传入参数，node参数的命名空间，可以为空 |

返回值描述

|   返回值    | 类型 |          描述          |
| :---------: | :--: | :--------------------: |
| return bool | bool | 返回设置参数值是否成功 |

接口描述

此接口为接口特化，针对参数类型为int64_t进行值的设置



##### 10.rclc_param_set_parameters_double

参数描述

|     参数名      |     类型      | 是否必须 |                  描述                  |
| :-------------: | :-----------: | :------: | :------------------------------------: |
|      node       |  rcl_node_t*  |   必须   |         传入参数，传入一个节点         |
|     client      | rcl_client_t* |   必须   |        传入参数，传入一个client        |
|    node_name    |     char*     |   必须   |          传入参数，传入节点名          |
|   remote_name   |     char*     |   必须   |        传入参数，service节点名         |
| parameter_name  |     char*     |   必须   |            传入参数，参数名            |
| parameter_value |    double     |   必须   |            传入参数，参数值            |
| node_name_space |     char*     |   必须   | 传入参数，node参数的命名空间，可以为空 |

返回值描述

|   返回值    | 类型 |          描述          |
| :---------: | :--: | :--------------------: |
| return bool | bool | 返回设置参数值是否成功 |

接口描述

此接口为接口特化，针对参数类型为double进行值的设置



##### 11.rclc_param_set_parameters_string

参数描述

|     参数名      |     类型      | 是否必须 |                  描述                  |
| :-------------: | :-----------: | :------: | :------------------------------------: |
|      node       |  rcl_node_t*  |   必须   |         传入参数，传入一个节点         |
|     client      | rcl_client_t* |   必须   |        传入参数，传入一个client        |
|    node_name    |     char*     |   必须   |          传入参数，传入节点名          |
|   remote_name   |     char*     |   必须   |        传入参数，service节点名         |
| parameter_name  |     char*     |   必须   |            传入参数，参数名            |
| parameter_value |     char*     |   必须   |            传入参数，参数值            |
| node_name_space |     char*     |   必须   | 传入参数，node参数的命名空间，可以为空 |

返回值描述

|   返回值    | 类型 |          描述          |
| :---------: | :--: | :--------------------: |
| return bool | bool | 返回设置参数值是否成功 |

接口描述

此接口为接口特化，针对参数类型为char*进行值的设置



##### 12.rclc_param_set_parameters_bool

参数描述

|     参数名      |     类型      | 是否必须 |                  描述                  |
| :-------------: | :-----------: | :------: | :------------------------------------: |
|      node       |  rcl_node_t*  |   必须   |         传入参数，传入一个节点         |
|     client      | rcl_client_t* |   必须   |        传入参数，传入一个client        |
|    node_name    |     char*     |   必须   |          传入参数，传入节点名          |
|   remote_name   |     char*     |   必须   |        传入参数，service节点名         |
| parameter_name  |     char*     |   必须   |            传入参数，参数名            |
| parameter_value |     bool      |   必须   |            传入参数，参数值            |
| node_name_space |     char*     |   必须   | 传入参数，node参数的命名空间，可以为空 |

返回值描述

|   返回值    | 类型 |          描述          |
| :---------: | :--: | :--------------------: |
| return bool | bool | 返回设置参数值是否成功 |

接口描述

此接口为接口特化，针对参数类型为bool进行值的设置



##### 13.rclc_on_parameter_event

|   参数名    |      类型       | 是否必须 |                             描述                             |
| :---------: | :-------------: | :------: | :----------------------------------------------------------: |
| msg_request | Parameter_Set_* |   必须   | 传入传出参数，传入参数名，其他节点的节点名，即可获得实时你关注参数的值，类型描述上述有请查阅 |
|    size     |     size_t      |   必须   |       msg_request的大小，必须小于等于msg_request的size       |

Parameter_Set_ 描述

|     参数名      |  类型  | 是否必须 |          描述          |
| :-------------: | :----: | :------: | :--------------------: |
| parameter_name  | char*  |    是    |         参数名         |
|    node_name    | char*  |    是    |    关注节点的节点名    |
| Basic_Data_Set_ | 结构体 |    是    | 参数值结构体，如下所诉 |



Basic_Data_Set_描述

|        参数名        |   类型   | 是否必须 |             描述             |
| :------------------: | :------: | :------: | :--------------------------: |
|      bool_value      |   bool   |    否    |  传出参数，参数bool类型的值  |
|    interger_value    | int64_t  |    否    |  传出参数，参数int类型的值   |
|     double_value     |  double  |    否    | 传出参数，参数double类型的值 |
|     string_value     |  char*   |    否    | 传出参数，参数char*类型的值  |
|   byte_array_value   | uint8_t* |    否    |          暂时未使用          |
|   bool_array_value   |  bool*   |    否    |          暂时未使用          |
| interger_array_value | int64_t* |    否    |          暂时未使用          |
|  double_array_value  | double*  |    否    |          暂时未使用          |
|  string_array_value  |  char**  |    否    |          暂时未使用          |

接口描述

此接口是用于实时更新关注其他节点参数的值！实现方式为订阅topic（/parameters_event)



时序图如下：

![image-20210907163134813](./images/client.png)
